package Server;

import java.io.IOException;
import java.util.Scanner;

public class ServerLauncher {
    public static void main(String[] args) {
        try {
            Server server = new Server(Server.port, 10);
            Thread serverThread = new Thread(server);
            serverThread.start();

            Scanner scanner = new Scanner(System.in);
            // Check for input to close the server
            if (scanner.hasNextLine() && scanner.nextLine().equalsIgnoreCase("close")) {
                serverThread.interrupt();
            }
            scanner.close();
        } catch (IOException e) {
            System.err.println("Error starting server: " + e.getMessage());
        }
    }
}
