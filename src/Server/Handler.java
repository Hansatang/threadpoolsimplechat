package Server;

import java.io.*;
import java.net.Socket;

public class Handler implements Runnable {
    private final Socket socket;
    private final PrintWriter out;
    private final BufferedReader input;
    private final HandlerManager handlerManager;

    Handler(Socket socket, HandlerManager handlerManager) throws IOException {
        this.socket = socket;
        this.handlerManager = handlerManager;
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream());

        out.println("Hi! Welcome to chat.");

        handlerManager.addHandler(this);
    }

    public void run() {
        try {
            String lastMessage;
            while ((lastMessage = input.readLine()) != null && !lastMessage.equals("quit")) {
                handlerManager.broadcastMessage(lastMessage, this);
            }

            out.println("Quitting...");

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                out.close();
                handlerManager.removeHandler(this);
                socket.close();
            } catch (IOException e) {
                System.err.println("IO exception occurred during closeout");
            }
        }
    }

    public synchronized void sendMessage(String m) {
        out.println(m);
    }
}
