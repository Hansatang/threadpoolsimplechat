package Server;

import java.util.ArrayList;
import java.util.List;

public class HandlerManager {
    private final List<Handler> allClients = new ArrayList<>();

    public void addHandler(Handler handler) {
        allClients.add(handler);
    }

    public void removeHandler(Handler handler) {
        allClients.remove(handler);
    }

    public void broadcastMessage(String message, Handler sender) {
        for (Handler handler : allClients) {
            if (!handler.equals(sender)) {
                handler.sendMessage(message);
            }
        }
    }
}
