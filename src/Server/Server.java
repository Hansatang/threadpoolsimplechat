package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {
    private final ServerSocket serverSocket;
    private final ExecutorService pool;
    private final HandlerManager handlerManager;
    static final int port = 12344;

    public Server(int port, int poolSize) throws IOException {
        System.out.println("Starting server");
        serverSocket = new ServerSocket(port);
        pool = Executors.newFixedThreadPool(poolSize);
        handlerManager = new HandlerManager();
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                pool.execute(new Handler(serverSocket.accept(), handlerManager));
                System.out.println("Another");
            }

        } catch (IOException ex) {
            System.err.println("IO exception occurred");
        } finally {
            closeServer();
        }
    }

    public void closeServer() {
        pool.shutdown();
        try {
            serverSocket.close();
        } catch (IOException e) {
            System.err.println("IO exception occurred while closing socket server");
        }
    }
}