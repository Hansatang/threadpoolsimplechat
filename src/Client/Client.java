package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {
    private final Socket socket;
    private final PrintStream out;
    private final Scanner input;
    private final ExecutorService pool;

    public Client(String serverAddress, int serverPort) throws IOException {
        this.socket = new Socket(serverAddress, serverPort);
        this.out = new PrintStream(socket.getOutputStream());
        this.input = new Scanner(socket.getInputStream());

        pool = Executors.newCachedThreadPool();
        pool.execute(this::receiveMessages);
        pool.execute(this::sendMessages);
    }

    public void sendMessage(String message) {
        out.println(message);
    }

    private void receiveMessages() {
        try {
            while (!Thread.interrupted()) {
                if (input.hasNextLine()) {
                    System.out.println("Received: " + input.nextLine());
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            close();
        }
    }

    private void sendMessages() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while (Thread.interrupted()) {
                sendMessage(reader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public void close() {
        try {
            pool.shutdown();
            out.close();
            input.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}